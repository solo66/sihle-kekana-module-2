void main() {
  //String List array to store the previous winners of the MTN App of the year from 2012 to 2021. Used a string list rather than a untyped array
  //due to the methods defined on the List<String> class. Apps listed in the order which they won the award (e.g. FNB Banking - 2012 winner, etc).
  List<String> winners = [
    "FNB Banking App",
    "Snapscan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula",
    "Naked Insurance",
    "EasyEquities",
    "Ambani Africa"
  ];

  //Makes a copy of the winning list since the sort method changes the order
  List<String> copy = winners;

  //Calls the sort method to sort the list alphabetically.
  winners.sort(
    (a, b) {
      return a.compareTo(b);
    },
  );

  //The variables that will store the 2017, 2018 and the number of winners respectively.
  var winner_2017 = copy[5];
  var winner_2018 = copy[6];
  var winnersNum = winners.length;

  //The statements to output the data to the console.
  print("The sorted list is: $winners");
  print(
      "The winning app of 2017 is: $winner_2017 \nThe winning app of 2018 is: $winner_2018");
  print("The total number of apps in the array is $winnersNum.");
}
