//The definition of the class to store the data on the MTN app.
class MTNAPP {
  String? app_name;
  String? app_category;
  String? app_developer;
  int? year_winner;

  //The constructor of the class.
  MTNAPP(name, category, developer, year) {
    app_name = name;
    app_category = category;
    app_developer = developer;
    year_winner = year;
  }

  //The member function that prints out the details of the MTN App Object to the console.
  void printAppInfo() {
    print(
        "App name: $app_name \nApp Caregory: $app_category \nApp Developer: $app_developer \nYear award won: $year_winner \n");
  }

  //The member function that changes the letters of the App name to All Caps.
  void nameToCaps() {
    if (app_name is String) {
      app_name = app_name!.toUpperCase();
    }
  }
}

void main() {
  //Instantiation of the class.
  var snapscan = new MTNAPP(
      "Snapscan", "Overall Winner", "Estiaan (and four others)", 2013);

  snapscan.printAppInfo(); //Prints the info of the app.
  snapscan.nameToCaps(); //Changes the letters in the name to all CAPS.
  print(snapscan.app_name); //Outputs the new name.
}
