void main() {
  //Storage of my data for my name, favorite city and my current city of residence.
  var user_name = "Sihle Kekana";
  var fav_city = "Salt Lake City";
  var user_city = "Johannesburg";

  //Statement to print out my user data that has been stored in the above variables.
  print(
      "Your name is: \t$user_name\nYour favority city is: \t$fav_city\nYour city is:\t$user_city\n");
}
